from django.db import models

from datetime import datetime
import pytz

class Keyword(models.Model):
    word = models.TextField()
    last_scan = models.DateTimeField(blank = True, null = True, editable = True)

    def __str__(self):
        return self.word

    def setLastScan(self):
        self.last_scan = datetime.now(pytz.timezone('UTC'))
        print(self.word, self.last_scan)
        self.save()

class GooglePositionReq(models.Model):
    word = models.ForeignKey(Keyword, on_delete = 'CASCADE')
    task_id = models.CharField(max_length = 32)
    timestamp_start = models.DateTimeField(auto_now_add = True, editable = False)
    timestamp_end = models.DateTimeField(editable = True, blank = True, null = True)

    def __str__(self):
        return (self.word.word + ' ' + self.task_id)

    def setTimestampEnd(self):
        self.timestamp_end = datetime.now(pytz.timezone('UTC'))
        print(self.task_id, self.timestamp_end)
        self.save()

class ElementPositions(models.Model):
    GPR = models.ForeignKey(GooglePositionReq, on_delete = 'CASCADE')
    position = models.PositiveSmallIntegerField()
    www = models.BooleanField()
    domain = models.TextField()
    path = models.TextField()
    title = models.TextField()

    def __str__(self):
        return self.domain

class Domain(models.Model):
    domain = models.TextField()

    def __str__(self):
        return self.domain

#------------------BackLinks-------------------------------------------

class BackLinks(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    domain_from = models.TextField()
    domain_to = models.TextField()
    link_text = models.TextField()
    nofollow = models.TextField()
    noindex = models.TextField()
    exlinks = models.TextField()
    redirect = models.TextField()
    count = models.TextField()
    tic = models.TextField()
    link_rank = models.TextField()
    domain_rank = models.TextField()
    trust_rank = models.TextField()
    level_from = models.TextField()
    ip = models.TextField()
    add = models.TextField()
    check = models.TextField()
    delete = models.TextField()
    yaca = models.TextField()
    seo_link = models.TextField()
    comm = models.TextField()

    def __str__(self):
        return self.domain_back

class BackLinksTotal(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    links = models.PositiveSmallIntegerField()
    mirrors = models.PositiveSmallIntegerField()
    links_unique = models.PositiveSmallIntegerField()
    zones_unique = models.PositiveSmallIntegerField()
    zones_to_unique = models.PositiveSmallIntegerField()
    anchors_unique = models.PositiveSmallIntegerField()
    words = models.PositiveSmallIntegerField()
    words_unique = models.PositiveSmallIntegerField()
    links_dofollow_total = models.PositiveSmallIntegerField()
    links_noindex_total = models.PositiveSmallIntegerField()
    links_nofollow_total = models.PositiveSmallIntegerField()
    donors_subnets = models.PositiveSmallIntegerField()
    donors_ips = models.PositiveSmallIntegerField()
    donors_domains = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.domain_back

class BackLinksTotalDonors(models.Model):
    domain = models.ForeignKey(BackLinksTotal, on_delete = models.CASCADE)
    name = models.TextField()
    count = models.TextField()

    def __str__(self):
        return self.domain

class BackLinksChanges(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    changes_date = models.DateField()
    domain_new = models.IntegerField()
    domain_delete = models.TextField()
    domain_total = models.TextField()

    def __str__(self):
        return self.domain_back

#---------------OutLinks----------------------------------------

class OutLinks(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    domain_from = models.TextField()
    domain_to = models.TextField()
    link_text = models.TextField()
    nofollow = models.TextField()
    noindex = models.TextField()
    exlinks = models.TextField()
    redirect = models.TextField()
    count = models.TextField()
    tic = models.TextField()
    link_rank = models.PositiveIntegerField(blank=True, null=True)
    domain_rank = models.PositiveIntegerField(blank=True, null=True)
    trust_rank = models.PositiveIntegerField(blank=True, null=True)
    level_from = models.TextField()
    ip = models.TextField()
    add = models.TextField()
    check = models.TextField()
    delete = models.TextField()

    def __str__(self):
        return self.domain_back

class OutLinksTotal(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    links = models.PositiveIntegerField()
    mirrors = models.PositiveSmallIntegerField()
    links_unique = models.PositiveIntegerField()
    zones_unique = models.PositiveSmallIntegerField()
    zones_to_unique = models.PositiveSmallIntegerField()
    anchors_unique = models.PositiveIntegerField()
    words = models.PositiveIntegerField()
    words_unique = models.PositiveIntegerField()
    links_nofollow_total = models.PositiveIntegerField()
    links_dofollow_total = models.PositiveIntegerField()
    links_noindex_total = models.PositiveSmallIntegerField()
    seo_percent = models.PositiveSmallIntegerField(blank=True, null=True)
    org_percent = models.PositiveSmallIntegerField(blank=True, null=True)

    donors_subnets = models.PositiveSmallIntegerField()
    donors_ips = models.PositiveSmallIntegerField()
    donors_domains = models.PositiveSmallIntegerField()

    acceptors_domains = models.IntegerField()
    acceptors_paths = models.IntegerField()

    def __str__(self):
        return self.domain_back

class OutLinksTotalDonors(models.Model):
    domain = models.ForeignKey(OutLinksTotal, on_delete = models.CASCADE)
    d_name = models.TextField()
    d_count = models.TextField()

    def __str__(self):
        return self.domain

class OutLinksTotalAcceptors(models.Model):
    domain = models.ForeignKey(OutLinksTotal, on_delete = models.CASCADE)
    a_name = models.TextField()
    a_count = models.TextField()

    def __str__(self):
        return self.domain

class BackLinksCounters(models.Model):
    domain_back = models.ForeignKey(Domain, on_delete = models.CASCADE)
    ips = models.IntegerField()
    subnets = models.IntegerField()
    domains = models.IntegerField()
    paths_to = models.IntegerField()
    indexed_pages = models.TextField()
    trust_rank_log = models.TextField()
    links = models.IntegerField()
    link_rank = models.TextField()
    elinks = models.TextField()
    elink_domains = models.TextField()
    elinks_uniq = models.TextField()
    tic = models.TextField()
    subs = models.IntegerField()
    domain_rank_log = models.TextField()
    seo_percent = models.IntegerField()
    org_percent = models.IntegerField()
    links_uniq = models.IntegerField()
    elinks_anchorless = models.TextField()

    def __str__(self):
        return self.domain_back
