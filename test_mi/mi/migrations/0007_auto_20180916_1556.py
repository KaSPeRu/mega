# Generated by Django 2.1.1 on 2018-09-16 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mi', '0006_backlinkstotal'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackLinksTotalDonors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('count', models.TextField()),
            ],
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_by',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_com',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_info',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_me',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_net',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_org',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_ru',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_su',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_tech',
        ),
        migrations.RemoveField(
            model_name='backlinkstotal',
            name='donors_zones_ua',
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='add',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='check',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='count',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='delete',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='domain_rank',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='exlinks',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='ip',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='level_from',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='link_rank',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='nofollow',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='noindex',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='redirect',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='seo_link',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='tic',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='backlinks',
            name='trust_rank',
            field=models.TextField(),
        ),
        migrations.AddField(
            model_name='backlinkstotaldonors',
            name='domain',
            field=models.ForeignKey(on_delete='CASCADE', to='mi.BackLinksTotal'),
        ),
    ]
