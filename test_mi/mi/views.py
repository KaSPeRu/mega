
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from .models import Keyword, GooglePositionReq, ElementPositions, Domain, BackLinks, BackLinksTotal, BackLinksTotalDonors, BackLinksChanges, OutLinks, OutLinksTotal, OutLinksTotalDonors, OutLinksTotalAcceptors, BackLinksCounters
from datetime import datetime
import pytz, requests, time

def index(request):
    keywords = Keyword.objects.all().order_by('-last_scan')
    pages = Paginator(keywords, 100)

    context = {'keywords_on_page': pages.get_page((request.GET.get('page')))}
    return render(request, 'html/index.html', context)

def keyword(request, word):
    key = Keyword.objects.filter(word = word).first()
    GPRs = GooglePositionReq.objects.filter(word = key)
    pages = Paginator(GPRs, 100)

    context = {'gprs_on_page': pages.get_page((request.GET.get('page')))}
    return render(request, 'html/keyword.html', context)

def task(request, word, task_id):
    GPR = GooglePositionReq.objects.filter(task_id = task_id).first()
    positions = ElementPositions.objects.filter(GPR = GPR)

    context = {'positions': positions}
    return render(request, 'html/task.html', context)

#-----------------------------------------------------------------------------

def domain_check(request):
    domain = Domain.objects.all()
    return render(request, "html/domain_check.html", {"domain": domain})

def domain_create(request):
    if request.method == "POST":
        domain = Domain()
        domain.domain = request.POST.get("domain")
        domain.save()
    return HttpResponseRedirect("/domaincheck")

def domain_delete(request, id):
    try:
        domain = Domain.objects.get(id=id)
        domain.delete()
        return HttpResponseRedirect("/domaincheck")
    except Domain.DoesNotExist:
        return HttpResponseNotFound("<h2>Domain not found</h2>")

#----------------------------BackLinks----------------------------------------

def domain_info(request, domain):
    BACK_LINKS_URL = 'http://api.megaindex.com/backlinks'
    BACK_LINKS_TOTAL_URL = 'http://api.megaindex.com/backlinks/total'
    BACK_LINKS_CHANGES_URL = 'http://api.megaindex.com/backlinks/changes'
    BACK_LINKS_COUNTERS_URL = 'http://api.megaindex.com/backlinks/counters'
    LINKS_PARAMS = {
            'key': '0d0a93a3032fef539691dfd04d2a11cb',
        }
    domain_key = Domain.objects.get(domain = domain)
    LINKS_PARAMS.update({'domain': domain})
    req1 = requests.get(BACK_LINKS_URL, LINKS_PARAMS)
    req2 = requests.get(BACK_LINKS_TOTAL_URL, LINKS_PARAMS)
    req3 = requests.get(BACK_LINKS_CHANGES_URL, LINKS_PARAMS)
    req4 = requests.get(BACK_LINKS_COUNTERS_URL, LINKS_PARAMS)
    reqj = req1.json()
    reqk = req2.json()
    reqc = req3.json()
    reqs = req4.json()

    for item in reqj['data']:
        backlinks = BackLinks(domain_back = domain_key)
        backlinks.domain_from = item['domain_from']
        backlinks.domain_to = item['domain_to']
        backlinks.link_text = item['link_text']
        backlinks.nofollow = item['nofollow']
        backlinks.noindex = item['noindex']
        backlinks.exlinks = item['exlinks']
        backlinks.redirect = item['redirect']
        backlinks.count = item['count']
        backlinks.tic = item['tic']
        backlinks.link_rank = item['link_rank']
        backlinks.domain_rank = item['domain_rank']
        backlinks.trust_rank = item['trust_rank']
        backlinks.level_from = item['level_from']
        backlinks.ip = item['ip']
        backlinks.add = item['add']
        backlinks.check = item['check']
        backlinks.delete = item['del']
        backlinks.yaca = item['yaca']
        backlinks.seo_link = item['seo_link']
        backlinks.comm = item['comm']
        backlinks.save()
    backlinks = BackLinks.objects.filter(domain_back = domain_key)

    backlinkstotal = BackLinksTotal(domain_back = domain_key)
    backlinkstotal.links = reqk['data']['total']['links']
    backlinkstotal.mirrors = reqk['data']['total']['mirrors']
    backlinkstotal.links_unique = reqk['data']['total']['links_unique']
    backlinkstotal.zones_unique = reqk['data']['total']['zones_unique']
    backlinkstotal.zones_to_unique = reqk['data']['total']['zones_to_unique']
    backlinkstotal.anchors_unique = reqk['data']['total']['anchors_unique']
    backlinkstotal.words = reqk['data']['total']['words']
    backlinkstotal.words_unique = reqk['data']['total']['words_unique']
    backlinkstotal.links_dofollow_total = reqk['data']['total']['links_dofollow_total']
    backlinkstotal.links_noindex_total = reqk['data']['total']['links_noindex_total']
    backlinkstotal.links_nofollow_total = reqk['data']['total']['links_nofollow_total']
    backlinkstotal.donors_subnets = reqk['data']['donors']['subnets']
    backlinkstotal.donors_ips = reqk['data']['donors']['ips']
    backlinkstotal.donors_domains = reqk['data']['donors']['domains']
    backlinkstotal.save()

    domain_donors = BackLinksTotal.objects.get(domain_back = domain_key)
    for item in reqk['data']['donors']['zones']:
        donor = BackLinksTotalDonors(domain = domain_donors)
        donor.name = item['z']
        donor.count = item['c']
        donor.save()

    donors = BackLinksTotalDonors.objects.filter(domain = domain_donors)
    for item in reqc['data']:
        changes = BackLinksChanges(domain_back = domain_key)
        changes.changes_date = item
        changes.domain_new = reqc['data'][str(item)]['new']
        changes.domain_delete = reqc['data'][str(item)]['del']
        changes.domain_total = reqc['data'][str(item)]['tot']
        changes.save()
    changess = BackLinksChanges.objects.filter(domain_back = domain_key).order_by('-changes_date')

#-----------------------------OutLinks----------------------------------------

    OUT_LINKS_URL = 'http://api.megaindex.com/outlinks'
    OUT_LINKS_TOTAL_URL = 'http://api.megaindex.com/outlinks/total'

    req4 = requests.get(OUT_LINKS_URL, LINKS_PARAMS)
    req5 = requests.get(OUT_LINKS_TOTAL_URL, LINKS_PARAMS)

    reqm = req4.json()
    reqn = req5.json()


    for item in reqm['data']:
        outlinks = OutLinks(domain_back = domain_key)
        outlinks.domain_from = item['domain_from']
        outlinks.domain_to = item['domain_to']
        outlinks.link_text = item['link_text']
        outlinks.nofollow = item['nofollow']
        outlinks.noindex = item['noindex']
        outlinks.exlinks = item['exlinks']
        outlinks.redirect = item['redirect']
        outlinks.count = item['count']
        outlinks.tic = item['tic']
        outlinks.link_rank = item['link_rank']
        outlinks.domain_rank = item['domain_rank']
        outlinks.trust_rank = item['trust_rank']
        outlinks.level_from = item['level_from']
        outlinks.ip = item['ip']
        outlinks.add = item['add']
        outlinks.check = item['check']
        outlinks.delete = item['del']
        outlinks.save()
    outlinks = OutLinks.objects.filter(domain_back = domain_key)

    outlinkstotal = OutLinksTotal(domain_back = domain_key)
    outlinkstotal.links = reqn['data']['total']['links']
    outlinkstotal.mirrors = reqn['data']['total']['mirrors']
    outlinkstotal.links_unique = reqn['data']['total']['links_unique']
    outlinkstotal.zones_unique = reqn['data']['total']['zones_unique']
    outlinkstotal.zones_to_unique = reqn['data']['total']['zones_to_unique']
    outlinkstotal.anchors_unique = reqn['data']['total']['anchors_unique']
    outlinkstotal.words = reqn['data']['total']['words']
    outlinkstotal.words_unique = reqn['data']['total']['words_unique']
    outlinkstotal.links_dofollow_total = reqn['data']['total']['links_dofollow_total']
    outlinkstotal.links_noindex_total = reqn['data']['total']['links_noindex_total']
    outlinkstotal.links_nofollow_total = reqn['data']['total']['links_nofollow_total']
    outlinkstotal.donors_subnets = reqn['data']['donors']['subnets']
    outlinkstotal.donors_ips = reqn['data']['donors']['ips']
    outlinkstotal.donors_domains = reqn['data']['donors']['domains']
    outlinkstotal.acceptors_domains = reqn['data']['acceptors']['domains']
    outlinkstotal.acceptors_paths = reqn['data']['acceptors']['paths']
    outlinkstotal.save()

    domain_out_donors = OutLinksTotal.objects.get(domain_back = domain_key)
    for item in reqn['data']['donors']['zones']:
        out_donor = OutLinksTotalDonors(domain = domain_out_donors)
        out_donor.d_name = item['z']
        out_donor.d_count = item['c']
        out_donor.save()

    for items in reqn['data']['acceptors']['zones']:
        out_acceptors = OutLinksTotalAcceptors(domain = domain_out_donors)
        out_acceptors.a_name = items['z']
        out_acceptors.a_count = items['c']
        out_acceptors.save()

    out_donors = OutLinksTotalDonors.objects.filter(domain = domain_out_donors)
    out_acceptors = OutLinksTotalAcceptors.objects.filter(domain = domain_out_donors)

    counters = BackLinksCounters(domain_back = domain_key)
    counters.ips = reqs['data']['ips']
    counters.subnets = reqs['data']['subnets']
    counters.domains = reqs['data']['domains']
    counters.paths_to = reqs['data']['paths_to']
    counters.indexed_pages = reqs['data']['indexed_pages']
    counters.trust_rank_log = reqs['data']['trust_rank_log']
    counters.links = reqs['data']['links']
    counters.link_rank = reqs['data']['link_rank']
    counters.elinks = reqs['data']['elinks']
    counters.elink_domains = reqs['data']['elink_domains']
    counters.elinks_uniq = reqs['data']['elinks_uniq']
    counters.tic = reqs['data']['tic']
    counters.subs = reqs['data']['subs']
    counters.domain_rank_log = reqs['data']['domain_rank_log']
    counters.seo_percent = reqs['data']['seo_percent']
    counters.org_percent = reqs['data']['org_percent']
    counters.links_uniq = reqs['data']['links_uniq']
    counters.elinks_anchorless = reqs['data']['elinks_anchorless']
    counters.save()

    return render(request, "html/domain_info.html", {"backlinks": backlinks, "backlinkstotal": backlinkstotal, "donors": donors, "changess": changess, "outlinks": outlinks, "outlinkstotal": outlinkstotal, "out_donors": out_donors, "out_acceptors": out_acceptors, "counters": counters})

#-----------------------------------------------------------------------------

def full_stat(request):
    # keywords = Keyword.objects.all()
    GPRs = GooglePositionReq.objects.all().order_by('-timestamp_end')
    # EPs = ElementPositions.objects.all()

    average_time = 0
    mark_time = dict()
    success_req = 0

    for item in range(0, 11):
        mark_time.update({
            item: {
                'len': 0,
                'GPRs': [],
            }
        })

    for item in GPRs:
        EPs = ElementPositions.objects.filter(GPR = item)
        if len(EPs):
            task_time = (item.timestamp_end - item.timestamp_start).seconds / 60
            average_time += task_time
            success_req += 1
            if int(task_time / 10):
                mark_time[10]['len'] += 1
                mark_time[10]['GPRs'].append(item)
            else:
                mark_time[int(task_time)]['len'] += 1
                mark_time[int(task_time)]['GPRs'].append(item)

    average_time /= success_req

    for item in range(0, 11):
        if request.GET.get('num') and str(item) == request.GET.get('num'):
            mark_time[int(request.GET.get('num'))]['GPRs'] = Paginator(mark_time[int(request.GET.get('num'))]['GPRs'], 5)
            mark_time[int(request.GET.get('num'))]['GPRs'] = mark_time[int(request.GET.get('num'))]['GPRs'].get_page(request.GET.get('page'))
        else:
            mark_time[item]['GPRs'] = Paginator(mark_time[item]['GPRs'], 5).get_page(1)
    context = {
        'average_time': round(average_time, 2),
        'mark_time': mark_time,
        'success_req': success_req,
        'full_req': len(GPRs),
    }

    return render(request, 'html/stat.html', context)
