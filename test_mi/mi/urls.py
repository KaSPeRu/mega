from django.urls import path, re_path
from . import views
app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('info/<domain>', views.domain_info),
    path('domaincheck/', views.domain_check),
    path('domaincheck/create/', views.domain_create),
    path('domaincheck/delete/<int:id>/', views.domain_delete),
    path('stat/', views.full_stat, name='stat'),
    path('<word>/', views.keyword, name='keyword'),
    path('<word>/<task_id>', views.task, name='task'),
]
