from django.core.management.base import BaseCommand, CommandError
from mi.models import Keyword, GooglePositionReq, ElementPositions
from datetime import datetime

import pytz, requests, time

GOOGLE_POSITION_URL = 'http://api.megaindex.com/scanning/google_position'
GOOGLE_POSITION_PARAMS = {
        'key': '0d0a93a3032fef539691dfd04d2a11cb',
        'show_page': 0,
        'show_direct': 0,
        'hl':'iw',
        'near':'Tel Aviv',
        'cr':'countryIL',
        'lang':'iw',
        'gdomain':'co.il',
        'lr':131,
    }

def addWordToParams(word, params):
    tmpParams = params
    tmpParams.update({'word': word})
    return tmpParams

def addGPR(req, word):
    if req['status']:
        tmpGRP = GooglePositionReq(word = word)
        tmpGRP.task_id = req['data']['task_id']
        tmpGRP.save()
        word.setLastScan()
        return True
    return False


TIME_OUT_REQUEST = 24*60*60            # 5 hours
COUNT_LIMIT_REQUEST = 100

def sendToScan():
    keywords = Keyword.objects.all()
    added_to_scan = 0

    start = datetime.now(pytz.timezone('UTC'))
    for item in keywords:
        if bool(item.last_scan) and (start - item.last_scan).days > 1:
            try:
                req = requests.get(GOOGLE_POSITION_URL, addWordToParams(item.word, GOOGLE_POSITION_PARAMS))
                result = addGPR(req.json(), item)
                added_to_scan += int(result)
            except Exception as e:
                pass
        elif not bool(item.last_scan):
            try:
                req = requests.get(GOOGLE_POSITION_URL, addWordToParams(item.word, GOOGLE_POSITION_PARAMS))
                result = addGPR(req.json(), item)
                added_to_scan += int(result)
            except Exception as e:
                pass
        if added_to_scan == 100:
            break

class Command(BaseCommand):
    help = 'Start scanning process'

    def handle(self, *args, **options):

        while True:
            sendToScan()
            time.sleep(30*60)
