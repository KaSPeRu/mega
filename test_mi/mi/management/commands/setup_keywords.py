from django.core.management.base import BaseCommand, CommandError
from mi.models import Keyword
from datetime import datetime

import keywords, pytz

class Command(BaseCommand):
    help = 'Setup database by keywords.py - JSON file import from mysql'

    def handle(self, *args, **options):
        start = datetime.now(pytz.timezone('UTC'))
        self.stdout.write(('Start time {}'.format(start)))

        for item in range(0, 5000):
            new_keyword = Keyword(word = keywords.db_array[2]['data'][item]['KeywordName'])
            new_keyword.save()

        stop = datetime.now(pytz.timezone('UTC'))
        self.stdout.write(self.style.SUCCESS('Success operation in {}, timedelta {}'.format(stop, (stop-start))))
