from django.core.management.base import BaseCommand, CommandError
from mi.models import Keyword, GooglePositionReq, ElementPositions
from datetime import datetime

import pytz, requests, time

GOOGLE_POSITION_URL = 'http://api.megaindex.com/scanning/google_position'
GOOGLE_POSITION_PARAMS = {
        'key': '0d0a93a3032fef539691dfd04d2a11cb',
        'show_page': 0,
        'show_direct': 0,
        'hl':'iw',
        'near':'Tel Aviv',
        'cr':'countryIL',
        'lang':'iw',
        'gdomain':'co.il',
        'lr':131,
    }

def addWordToParams(word, params):
    tmpParams = params
    tmpParams.update({'word': word})
    return tmpParams

def addScanning(req, GPR):
    if req['data']['task_id'] != GPR.task_id:
        GPR.setTimestampEnd()
        return
    if req['status'] and req['data']['scanning'] :
        GPR.setTimestampEnd()
        for item in range(0, 100):
            tmpElement = ElementPositions(GPR = GPR)
            tmpElement.position = req['data']['positions'][item]['position']
            tmpElement.www = req['data']['positions'][item]['www']
            tmpElement.domain = req['data']['positions'][item]['domain']
            tmpElement.path = req['data']['positions'][item]['path']
            tmpElement.title = req['data']['positions'][item]['title']
            tmpElement.save()

def getFromScan():
    GPRs = GooglePositionReq.objects.exclude(timestamp_end__isnull = False)
    start = datetime.now(pytz.timezone('UTC'))

    for item in GPRs:
        try:
            req = requests.get(GOOGLE_POSITION_URL, addWordToParams(item.word.word, GOOGLE_POSITION_PARAMS))
            result = addScanning(req.json(), item)
        except Exception as e:
            pass


class Command(BaseCommand):
    help = 'Start scanning process'

    def handle(self, *args, **options):
        while True:
            getFromScan()
            time.sleep(2*60)
