from django.core.management.base import BaseCommand, CommandError
from mi.models import Keyword, GooglePositionReq, ElementPositions
import pytz
from datetime import datetime

class Command(BaseCommand):
    help = 'Clear keyword table'

    def handle(self, *args, **options):
        start = datetime.now(pytz.timezone('UTC'))

        GPRs = GooglePositionReq.objects.all()
        for item in GPRs:
            EPs = ElementPositions.objects.filter(GPR = item)
            for jitem in EPs:
                jitem.delete()
            item.delete()

        stop = datetime.now(pytz.timezone('UTC'))

        self.stdout.write(self.style.SUCCESS('Command time {}'.format((stop-start))))
