import requests

GOOGLE_POSITION_URL = 'http://api.megaindex.com/scanning/google_position'
GOOGLE_POSITION_PARAMS = {
        'key': '0d0a93a3032fef539691dfd04d2a11cb',
        'show_page': 0,
        'show_direct': 0,
        'hl':'iw',
        'near':'Tel Aviv',
        'cr':'countryIL',
        'lang':'iw',
        'gdomain':'co.il',
    }

def addWordToParams(word, params):
    tmpParams = params
    tmpParams.update({'word': word})
    print (tmpParams)
    return tmpParams

tmpParam = addWordToParams('אולמי דניאל אור יהודה מחיר מנה', GOOGLE_POSITION_PARAMS)
print(tmpParam)
a = requests.get(GOOGLE_POSITION_URL, params = tmpParam)

print(a.json())
