from django.contrib import admin
from .models import Keyword, GooglePositionReq, ElementPositions, Domain, BackLinks, BackLinksTotal, BackLinksTotalDonors, BackLinksChanges,  OutLinks, OutLinksTotal, OutLinksTotalDonors, OutLinksTotalAcceptors, BackLinksCounters
# Register your models here.

admin.site.register(Keyword)

admin.site.register(GooglePositionReq)

admin.site.register(ElementPositions)

admin.site.register(Domain)

admin.site.register(BackLinks)

admin.site.register(BackLinksTotal)

admin.site.register(BackLinksTotalDonors)

admin.site.register(BackLinksChanges)

admin.site.register(OutLinks)

admin.site.register(OutLinksTotal)

admin.site.register(OutLinksTotalDonors)

admin.site.register(OutLinksTotalAcceptors)

admin.site.register(BackLinksCounters)
